terraform {
  required_providers {
    libvirt = {
      source  = "hashicorp/libvirt"
      version = "1.0.0"
    }
  }
}

provider "libvirt"{
    uri = "qemu:///system"
}
