#
# worker nodes
#

resource "libvirt_volume" "worker_image" {
  count  = local.workers
  name   = "worker-${count.index + 1}"
  pool   = "default"
  source = "https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img"
  format = "qcow2"
}

resource "libvirt_volume" "worker" {
  count          = local.workers
  name           = "volume-${count.index + 1}"
  base_volume_id = element(libvirt_volume.worker_image.*.id, count.index)
  size           = var.worker_root_size
}

resource "libvirt_cloudinit_disk" "commoninit" {
  count     = local.workers
  name      = "k8s-worker-${count.index + 1}-commoninit.iso"
  pool      = "default"
  user_data = element(data.template_file.user_data.*.rendered, count.index + 1)
}


data "template_file" "user_data" {
  count    = local.workers
  template = file("${path.module}/kubernetes_cloudinit.cfg")
  vars = {
    hostname    = "k8s-worker${count.index + 1}"
    public_key  = var.public_key
    k8s_version = var.k8s_version
    k8s_minor   = var.k8s_minor
  }
}


resource "libvirt_domain" "worker" {
  count  = local.workers
  name   = "k8s-worker${count.index + 1}"
  memory = var.worker_mem
  vcpu   = var.worker_cpu

  disk {
    volume_id = element(libvirt_volume.worker.*.id, count.index + 1)
  }

  network_interface {
    network_name = "default"
  }

  cloudinit = element(libvirt_cloudinit_disk.commoninit.*.id, count.index + 1)

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = "true"
  }

  autostart = var.enable_autostart
}

terraform {
  required_version = ">= 0.14"
}
