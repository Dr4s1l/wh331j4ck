
module "k8s" {
	source           = "./k8s"
	public_key       = file("/home/yggdrasil/.ssh/libvirt.pub")
	masters_count    = 1
	master_mem       = 2048
	master_cpu       = 2

	workers_count    = 40
	worker_mem       = 1024
	worker_cpu       = 1

        k8s_version      = "1.20"
	k8s_minor        = "0"

	enable_autostart = true
}

output "ips" {
	value = module.k8s.ips
}
